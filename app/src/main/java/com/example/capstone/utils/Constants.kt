package com.example.capstone.utils

import com.example.capstone.domain.model.AdDTO

object Constants {
    const val API_KEY = "7754ef3c3751d04070c226b198665358"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_URL = "https://image.tmdb.org/t/p/original"
    const val DEFAULT_LANGUAGE = "ru-RU"
    val ad = AdDTO(
        "1Fit",
        "Абонемент на все виды спорта",
        "https://resources.cdn-kaspi.kz/shop/medias/sys_master/images/images/h4b/hf7/47592727773214/1fit-bezlimit-3-mesaca-101420202-1-Container.png"
    )
}
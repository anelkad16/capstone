package com.example.capstone.domain.model

data class AdDTO(
    val title: String,
    val description: String,
    val image: String
)

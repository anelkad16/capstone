package com.example.capstone.domain.usecase

import com.example.capstone.domain.repository.SavedMovieRepository
import javax.inject.Inject

class DeleteMovieUseCase @Inject constructor(
    private val savedMovieRepository: SavedMovieRepository,
) {

    suspend operator fun invoke(movieId: Int) = savedMovieRepository.deleteMovie(movieId)
}

package com.example.capstone.domain.usecase

import com.example.capstone.domain.model.MovieDetailsDTO
import com.example.capstone.domain.repository.MovieRepository
import com.example.capstone.utils.CommonResult
import javax.inject.Inject

class GetMovieUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
) {

    suspend operator fun invoke(movieId: Int): CommonResult<MovieDetailsDTO> =
        movieRepository.getMovie(movieId)
}

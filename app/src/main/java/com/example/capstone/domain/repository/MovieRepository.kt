package com.example.capstone.domain.repository

import com.example.capstone.domain.model.ListItem
import com.example.capstone.domain.model.MovieDetailsDTO
import com.example.capstone.utils.CommonResult

interface MovieRepository {
    suspend fun getMovie(movieId: Int): CommonResult<MovieDetailsDTO>
    suspend fun getMovieList(page: Int): CommonResult<List<ListItem>>
}
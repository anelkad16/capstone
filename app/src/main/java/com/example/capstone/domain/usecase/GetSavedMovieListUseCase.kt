package com.example.capstone.domain.usecase

import com.example.capstone.domain.repository.SavedMovieRepository
import javax.inject.Inject

class GetSavedMovieListUseCase @Inject constructor(
    private val savedMovieRepository: SavedMovieRepository,
) {

    suspend operator fun invoke() = savedMovieRepository.getSavedMovieList()
}

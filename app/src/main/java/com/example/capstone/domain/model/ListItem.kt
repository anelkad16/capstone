package com.example.capstone.domain.model

sealed class ListItem {

    data class MovieItem(val movie: MovieDTO): ListItem()
    data class AdItem(val ad: AdDTO) : ListItem()
}

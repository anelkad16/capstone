package com.example.capstone.domain.repository

import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.utils.CommonResult

interface SavedMovieRepository {

    suspend fun getSavedMovieList(): CommonResult<List<MovieDTO>>
    suspend fun deleteMovie(movieId: Int): CommonResult<Int>
    suspend fun saveMovie(movie: MovieDTO): CommonResult<MovieDTO>
}

package com.example.capstone.domain.model

data class MovieDetailsDTO(
    val id: Int,
    val title: String,
    val originalTitle: String,
    val overview: String,
    val releaseDate: String,
    val posterPath: String,
    val backdropPath: String,
    val voteAverage: Float,
    val voteCount: Int,
    val productionCountries: List<ProductionCountryDTO>,
    val genres: List<GenreDTO>,
    val tagline: String,
    val revenue: Int,
    val runtime: Int
) {
    fun toMovie(): MovieDTO =
        MovieDTO(id, title, overview, releaseDate, posterPath, backdropPath, voteAverage)
}

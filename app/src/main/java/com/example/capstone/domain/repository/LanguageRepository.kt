package com.example.capstone.domain.repository

interface LanguageRepository {

    fun getLanguage(): String
    fun setLanguage(language: String)
}

package com.example.capstone.domain.model

data class ProductionCountryDTO(
    val iso_3166_1: String,
    val name: String
)
package com.example.capstone.domain.usecase

import com.example.capstone.domain.model.ListItem
import com.example.capstone.domain.repository.MovieRepository
import com.example.capstone.utils.CommonResult
import javax.inject.Inject

class GetMovieListUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
) {

    suspend operator fun invoke(page: Int): CommonResult<List<ListItem>> =
        movieRepository.getMovieList(page)
}

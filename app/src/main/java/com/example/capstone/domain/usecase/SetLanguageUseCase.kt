package com.example.capstone.domain.usecase

import com.example.capstone.domain.repository.LanguageRepository
import javax.inject.Inject

class SetLanguageUseCase @Inject constructor(
    private val languageRepository: LanguageRepository,
) {

    operator fun invoke(language: String) = languageRepository.setLanguage(language)
}

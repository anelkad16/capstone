package com.example.capstone.domain.usecase

import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.domain.repository.SavedMovieRepository
import javax.inject.Inject

class SaveMovieUseCase @Inject constructor(
    private val savedMovieRepository: SavedMovieRepository,
) {

    suspend operator fun invoke(movie: MovieDTO) = savedMovieRepository.saveMovie(movie)
}

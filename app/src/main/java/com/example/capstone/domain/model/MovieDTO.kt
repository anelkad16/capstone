package com.example.capstone.domain.model

data class MovieDTO(
    val id: Int = 0,
    val title: String,
    val overview: String?,
    val releaseDate: String?,
    val posterPath: String?,
    val backdropPath: String?,
    val voteAverage: Float?
)

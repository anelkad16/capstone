package com.example.capstone.domain.model

data class GenreDTO(
    val id: Int,
    val name: String
)

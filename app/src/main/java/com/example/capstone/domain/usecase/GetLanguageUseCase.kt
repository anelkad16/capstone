package com.example.capstone.domain.usecase

import com.example.capstone.domain.repository.LanguageRepository
import javax.inject.Inject

class GetLanguageUseCase @Inject constructor(
    private val languageRepository: LanguageRepository,
) {

    operator fun invoke() = languageRepository.getLanguage()
}

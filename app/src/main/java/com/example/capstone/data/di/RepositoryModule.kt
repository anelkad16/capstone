package com.example.capstone.data.di

import com.example.capstone.data.repository.LanguageRepositoryImpl
import com.example.capstone.data.repository.MovieRepositoryImp
import com.example.capstone.data.repository.SavedMovieRepositoryImp
import com.example.capstone.domain.repository.LanguageRepository
import com.example.capstone.domain.repository.MovieRepository
import com.example.capstone.domain.repository.SavedMovieRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindsMovieRepository(imp: MovieRepositoryImp): MovieRepository

    @Binds
    @Singleton
    abstract fun bindsSavedMovieRepository(imp: SavedMovieRepositoryImp): SavedMovieRepository

    @Binds
    @Singleton
    abstract fun bindsLanguageRepository(imp: LanguageRepositoryImpl): LanguageRepository
}

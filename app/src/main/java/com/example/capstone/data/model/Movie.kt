package com.example.capstone.data.model

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.capstone.domain.model.MovieDTO
import com.google.gson.annotations.SerializedName

@Keep
@Entity(tableName = "movie_table")
data class Movie(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "title")
    val title: String,
    val overview: String?,
    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    val releaseDate: String?,
    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @SerializedName("vote_average")
    val voteAverage: Float?
) {
    fun toDomain(): MovieDTO =
        MovieDTO(
            id = id,
            title = title,
            overview = overview,
            releaseDate = releaseDate,
            posterPath = posterPath,
            backdropPath = backdropPath,
            voteAverage = voteAverage
        )
}

fun MovieDTO.toDTO() = Movie(
    id = id,
    title = title,
    overview = overview,
    releaseDate = releaseDate,
    posterPath = posterPath,
    backdropPath = backdropPath,
    voteAverage = voteAverage
)

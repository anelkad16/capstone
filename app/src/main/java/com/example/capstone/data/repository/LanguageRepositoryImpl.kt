package com.example.capstone.data.repository

import com.example.capstone.data.local.LanguageStorage
import com.example.capstone.domain.repository.LanguageRepository
import javax.inject.Inject

class LanguageRepositoryImpl @Inject constructor(
    private val storage: LanguageStorage
): LanguageRepository {

    override fun getLanguage(): String {
        return storage.getLanguage()
    }

    override fun setLanguage(language: String) {
        storage.setLanguage(language)
    }
}

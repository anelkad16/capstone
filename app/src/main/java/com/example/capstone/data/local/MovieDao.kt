package com.example.capstone.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.capstone.data.model.Movie

@Dao
interface MovieDao {

    @Query("SELECT * FROM movie_table")
    fun getAll(): List<Movie>

    @Insert
    fun insertAll(vararg notes: Movie)

    @Query("DELETE FROM movie_table WHERE id = :id")
    fun delete(id: Int)
}

package com.example.capstone.data.repository

import com.example.capstone.data.local.MovieDatabase
import com.example.capstone.data.model.toDTO
import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.domain.repository.SavedMovieRepository
import com.example.capstone.utils.CommonResult
import javax.inject.Inject

class SavedMovieRepositoryImp @Inject constructor(
    private val database: MovieDatabase
) : SavedMovieRepository {

    override suspend fun getSavedMovieList(): CommonResult<List<MovieDTO>> {
        return try {
            CommonResult(result = database.noteDao().getAll().map { it.toDomain() })
        } catch (e: Exception) {
            CommonResult(error = e.message)
        }
    }

    override suspend fun deleteMovie(movieId: Int): CommonResult<Int> {
        return try {
            database.noteDao().delete(movieId)
            CommonResult(result = movieId)
        } catch (e: Exception) {
            CommonResult(error = e.message)
        }
    }

    override suspend fun saveMovie(movie: MovieDTO): CommonResult<MovieDTO> {
        return try {
            database.noteDao().insertAll(movie.toDTO())
            CommonResult(result = movie)
        } catch (e: Exception) {
            CommonResult(error = e.message)
        }
    }
}

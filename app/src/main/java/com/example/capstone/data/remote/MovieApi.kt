package com.example.capstone.data.remote

import com.example.capstone.data.model.MovieDetails
import com.example.capstone.data.model.MovieListResponse
import com.example.capstone.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @GET("discover/movie")
    suspend fun getMovieList(
        @Query("page")
        page: Int = 1,
        @Query("api_key")
        apiKey: String = Constants.API_KEY,
        @Query("with_genres")
        withGenres: String = "16,18",
        @Query("language")
        language: String
    ): Response<MovieListResponse>

    @GET("movie/{movie_id}")
    suspend fun getMovie(
        @Path("movie_id")
        movieId: Int,
        @Query("api_key")
        apiKey: String = Constants.API_KEY,
        @Query("language")
        language: String
    ): Response<MovieDetails>
}
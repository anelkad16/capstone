package com.example.capstone.data.model

import androidx.annotation.Keep
import com.example.capstone.domain.model.ProductionCountryDTO

@Keep
data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
) {
    fun toDomain(): ProductionCountryDTO = ProductionCountryDTO(
        iso_3166_1 = iso_3166_1,
        name = name
    )
}
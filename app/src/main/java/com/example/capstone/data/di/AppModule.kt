package com.example.capstone.data.di

import android.content.Context
import com.example.capstone.data.local.LanguageStorage
import com.example.capstone.data.local.MovieDatabase
import com.example.capstone.data.remote.MovieApi
import com.example.capstone.data.repository.MovieRepositoryImp
import com.example.capstone.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideContext(@ApplicationContext context: Context): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideMovieDatabase(
        context: Context
    ): MovieDatabase = MovieDatabase.getDatabase(context)

    @Provides
    @Singleton
    fun provideLanguageStorage(
        context: Context
    ): LanguageStorage = LanguageStorage(context)

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
        return client
    }

    @Provides
    @Singleton
    @Named("MOVIE_API")
    fun provideMovieApi(
        retrofit: Retrofit
    ): MovieApi = retrofit.create()

    @Provides
    @Singleton
    fun provideMovieRepository(
        @Named("MOVIE_API") movieApi: MovieApi,
        storage: LanguageStorage
    ): MovieRepositoryImp = MovieRepositoryImp(movieApi, storage)
}

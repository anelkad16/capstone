package com.example.capstone.data.repository

import com.example.capstone.data.local.LanguageStorage
import com.example.capstone.data.remote.MovieApi
import com.example.capstone.domain.model.ListItem
import com.example.capstone.domain.model.MovieDetailsDTO
import com.example.capstone.domain.repository.MovieRepository
import com.example.capstone.utils.CommonResult
import com.example.capstone.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieRepositoryImp @Inject constructor(
    private val api: MovieApi,
    private val storage: LanguageStorage
) : MovieRepository {
    override suspend fun getMovie(movieId: Int): CommonResult<MovieDetailsDTO> = withContext(Dispatchers.IO) {
        val response = api.getMovie(movieId, language = storage.getLanguage())
        if (response.isSuccessful) {
            CommonResult(result = response.body()?.toDomain())
        } else {
            CommonResult(error = response.message())
        }
    }

    override suspend fun getMovieList(page: Int): CommonResult<List<ListItem>> = withContext(Dispatchers.IO) {
        val response = api.getMovieList(page = page, language = storage.getLanguage())
        if (response.isSuccessful) {
            val list = buildList {
                response.body()?.results?.map { ListItem.MovieItem(it.toDomain()) }
                    ?.let { addAll(it) }
                add(10, ListItem.AdItem(Constants.ad))
            }
            CommonResult(result = list)
        } else {
            CommonResult(error = response.message())
        }
    }
}

package com.example.capstone.data.local

import android.content.Context
import android.content.SharedPreferences
import com.example.capstone.utils.Constants
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LanguageStorage @Inject constructor(
    val context: Context
) {
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("language_preferences", Context.MODE_PRIVATE)

    companion object {
        const val LANGUAGE_KEY = "language"
    }

    fun setLanguage(language: String) {
        sharedPreferences.edit().putString(LANGUAGE_KEY, language).apply()
    }

    fun getLanguage(): String {
        return sharedPreferences.getString(LANGUAGE_KEY, null)
            ?: Constants.DEFAULT_LANGUAGE
    }
}

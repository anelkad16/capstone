package com.example.capstone.data.model

import androidx.annotation.Keep
import com.example.capstone.domain.model.GenreDTO

@Keep
data class Genre(
    val id: Int,
    val name: String
) {
    fun toDomain(): GenreDTO = GenreDTO(
        id = id,
        name = name
    )
}
package com.example.capstone.ui.savedMovieList

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.capstone.R
import com.example.capstone.databinding.FragmentSavedMovieBinding
import com.example.capstone.ui.delegates.DialogDelegate
import com.example.capstone.ui.delegates.WaitDialogDelegate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SavedMovieFragment : Fragment(R.layout.fragment_saved_movie),
    DialogDelegate by WaitDialogDelegate() {

    private var binding: FragmentSavedMovieBinding? = null
    private val movieAdapter by lazy {
        SavedMovieAdapter(
            onItemClickListener = ::navigateToDetails,
            onDeleteMovieListener = ::onDeleteMovie
        )
    }
    private val viewModel: SavedMovieListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentSavedMovieBinding.bind(view)
        registerWaitDialogDelegate(this)
        bindViews()
        setupObservers()
    }

    private fun bindViews() {
        binding?.apply {
            listView.adapter = movieAdapter
            swipeRefresh.setOnRefreshListener {
                viewModel.getMovieList()
            }
        }
    }

    private fun setupObservers() {
        viewModel.state.onEach { state ->
            when (state) {
                is SavedMovieListViewModel.State.HideLoading -> {
                    binding?.progressBar?.isVisible = false
                }

                is SavedMovieListViewModel.State.ShowLoading -> {
                    binding?.progressBar?.isVisible = true
                }

                is SavedMovieListViewModel.State.SavedMovieList -> {
                    binding?.apply {
                        swipeRefresh.isRefreshing = false
                        noSavedMovie.isVisible = state.movies.isEmpty()
                    }
                    movieAdapter.submitList(state.movies)
                }
            }
        }.launchIn(lifecycleScope)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.effect.collect { effect ->
                when (effect) {
                    SavedMovieListViewModel.Effect.ShowWaitDialog -> {
                        showWaitDialog()
                    }
                    SavedMovieListViewModel.Effect.HideWaitDialog -> {
                        hideWaitDialog()
                    }
                    is SavedMovieListViewModel.Effect.MovieDeleted -> {
                        Toast.makeText(
                            context,
                            requireContext().getString(R.string.movie_deleted_title),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    is SavedMovieListViewModel.Effect.Error -> {
                        Toast.makeText(
                            context,
                            effect.error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }
    }

    private fun navigateToDetails(movieId: Int) {
        val bundle = Bundle().apply {
            putInt("id", movieId)
        }
        findNavController().navigate(
            R.id.action_savedMovieFragment_to_movieDetailsFragment,
            bundle
        )
    }

    private fun onDeleteMovie(movieId: Int) {
        viewModel.deleteMovie(movieId)
        viewModel.getMovieList()
    }
}

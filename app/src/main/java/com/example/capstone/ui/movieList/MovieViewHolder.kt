package com.example.capstone.ui.movieList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.capstone.R
import com.example.capstone.databinding.MovieItemBinding
import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.utils.Constants

class MovieViewHolder(
    private val movieItemBinding: MovieItemBinding,
    private val onMovieClickListener: (Int) -> Unit,
    private val saveMovieListener: (MovieDTO) -> Unit
) : RecyclerView.ViewHolder(movieItemBinding.root) {
    companion object {
        fun create(
            parent: ViewGroup,
            onMovieClickListener: ((Int) -> Unit),
            saveMovieListener: ((MovieDTO) -> Unit)
        ): MovieViewHolder {
            val binding = MovieItemBinding.inflate(
                LayoutInflater
                    .from(parent.context), parent, false
            )
            return MovieViewHolder(binding, onMovieClickListener, saveMovieListener)
        }
    }

    fun bind(movie: MovieDTO) {
        movieItemBinding.apply {
            title.text = movie.title
            description.text = description.context.getString(
                R.string.description,
                movie.voteAverage.toString(),
                movie.releaseDate
            )
            itemView.setOnClickListener { onMovieClickListener.invoke(movie.id) }
            saveButton.setOnClickListener { saveMovieListener.invoke(movie) }
        }
        Glide
            .with(movieItemBinding.imageView.context)
            .load(Constants.IMAGE_URL + movie.posterPath)
            .apply(RequestOptions.bitmapTransform(RoundedCorners(10)))
            .placeholder(R.drawable.progress_animation)
            .error(R.drawable.baseline_image_24)
            .into(movieItemBinding.imageView)
    }
}
package com.example.capstone.ui.movieList

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.capstone.R
import com.example.capstone.domain.model.ListItem
import com.example.capstone.domain.model.MovieDTO

class MovieListAdapter(
    private val onMovieClickListener: (Int) -> Unit,
    private val onSaveMovieListener: (MovieDTO) -> Unit
) : ListAdapter<ListItem, RecyclerView.ViewHolder>(MovieDiffCallback()) {
    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ListItem.MovieItem -> R.layout.movie_item
            is ListItem.AdItem -> R.layout.ad_item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.movie_item -> {
                MovieViewHolder.create(
                    parent = parent,
                    onMovieClickListener = onMovieClickListener,
                    saveMovieListener = onSaveMovieListener
                )
            }
            else -> {
                AdViewHolder.create(parent)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val listItem = getItem(position)
        listItem.let {
            when (listItem) {
                is ListItem.MovieItem -> {
                    (holder as MovieViewHolder).bind(listItem.movie)
                }

                is ListItem.AdItem -> (holder as AdViewHolder).bind(listItem.ad)
            }
        }
    }
}

class MovieDiffCallback : DiffUtil.ItemCallback<ListItem>() {
    override fun areItemsTheSame(oldItem: ListItem, newItem: ListItem): Boolean {
        val isSameMovieItem = oldItem is ListItem.MovieItem
                && newItem is ListItem.MovieItem
                && oldItem.movie.id == newItem.movie.id

        val isSameAdItem = oldItem is ListItem.AdItem
                && newItem is ListItem.AdItem
                && oldItem.ad == newItem.ad

        return isSameMovieItem || isSameAdItem
    }

    override fun areContentsTheSame(oldItem: ListItem, newItem: ListItem) = oldItem == newItem
}

package com.example.capstone.ui.movieList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.capstone.domain.model.ListItem
import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.domain.usecase.GetLanguageUseCase
import com.example.capstone.domain.usecase.GetMovieListUseCase
import com.example.capstone.domain.usecase.SaveMovieUseCase
import com.example.capstone.domain.usecase.SetLanguageUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MovieListViewModel @Inject constructor(
    private val getMovieListUseCase: GetMovieListUseCase,
    private val saveMovieUseCase: SaveMovieUseCase,
    private val setLanguageUseCase: SetLanguageUseCase,
    private val getLanguageUseCase: GetLanguageUseCase
) : ViewModel() {

    private var _state = MutableStateFlow<State>(State.ShowLoading)
    val state: StateFlow<State> = _state

    private val _effect: Channel<Effect> = Channel()
    val effect = _effect.receiveAsFlow()

    init {
        getMovieList(1)
    }

    private fun setEffect(effectValue: Effect) {
        viewModelScope.launch { _effect.send(effectValue) }
    }

    fun saveMovie(movie: MovieDTO) = viewModelScope.launch {
        setEffect(Effect.ShowWaitDialog)
        val response = withContext(Dispatchers.IO){
            saveMovieUseCase.invoke(movie)
        }
        response.result?.let {
            setEffect(Effect.MovieSaved(it))
        }
        response.error?.let {
            setEffect(Effect.Error(it))
        }
        setEffect(Effect.HideWaitDialog)
    }

    fun setLanguage(language: String) {
        setLanguageUseCase.invoke(language)
        getMovieList(1)
    }

    fun getLanguage() = getLanguageUseCase.invoke()

    fun getMovieList(page: Int) = viewModelScope.launch {
        val response = withContext(Dispatchers.IO) {
            val result = getMovieListUseCase(page)
            result
        }
        response.result?.let {
            _state.value = State.HideLoading
            _state.value = State.ShowMovieList(it)
        }
        response.error?.let {
            _state.value = State.HideLoading
            setEffect(Effect.Error(it))
        }
    }

    sealed class State {
        object ShowLoading : State()
        object HideLoading : State()
        data class ShowMovieList(val movieList: List<ListItem>) : State()
    }

    sealed interface Effect {
        object ShowWaitDialog : Effect
        data class MovieSaved(val movie: MovieDTO) : Effect
        object HideWaitDialog : Effect
        data class Error(val error: String) : Effect
    }
}

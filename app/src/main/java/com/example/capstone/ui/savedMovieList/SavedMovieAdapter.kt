package com.example.capstone.ui.savedMovieList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.capstone.R
import com.example.capstone.databinding.SavedItemBinding
import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.utils.Constants

class SavedMovieAdapter(
    private val onItemClickListener: (Int) -> Unit,
    private val onDeleteMovieListener: (Int) -> Unit,
) : ListAdapter<MovieDTO, SavedMovieHolder>(SavedMovieDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedMovieHolder {
        val binding = SavedItemBinding.inflate(
            LayoutInflater
                .from(parent.context), parent, false
        )
        return SavedMovieHolder(
            binding,
            onDeleteMovieListener,
            onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: SavedMovieHolder, position: Int) =
        holder.bind(getItem(position))
}

class SavedMovieHolder(
    private val binding: SavedItemBinding,
    private val onDeleteMovieListener: ((Int) -> Unit),
    private val onItemClickListener: (Int) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(movie: MovieDTO) {
        binding.apply {
            title.text = movie.title
            description.text = itemView.context.getString(R.string.premiere, movie.releaseDate)
            btnDelete.setOnClickListener { onDeleteMovieListener.invoke(movie.id) }
            itemView.setOnClickListener { onItemClickListener.invoke(movie.id) }
        }
        Glide
            .with(binding.imageView.context)
            .load(Constants.IMAGE_URL + movie.posterPath)
            .apply(RequestOptions.bitmapTransform(RoundedCorners(10)))
            .placeholder(R.drawable.progress_animation)
            .error(R.drawable.baseline_image_24)
            .into(binding.imageView)

    }
}

class SavedMovieDiffCallback : DiffUtil.ItemCallback<MovieDTO>() {
    override fun areItemsTheSame(oldItem: MovieDTO, newItem: MovieDTO) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: MovieDTO, newItem: MovieDTO) = oldItem == newItem
}
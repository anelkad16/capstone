package com.example.capstone.ui.movieDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.domain.model.MovieDetailsDTO
import com.example.capstone.domain.usecase.GetMovieUseCase
import com.example.capstone.domain.usecase.SaveMovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
    private val getMovieDetailsUseCase: GetMovieUseCase,
    private val saveMovieUseCase: SaveMovieUseCase
): ViewModel() {

    private var _state = MutableLiveData<State>(State.ShowLoading)
    val state: LiveData<State> = _state

    private val _effect: Channel<Effect> = Channel()
    val effect = _effect.receiveAsFlow()

    private fun setEffect(effectValue: Effect) {
        viewModelScope.launch { _effect.send(effectValue) }
    }

    fun getMovie(movieId: Int) = viewModelScope.launch {
        val response = withContext(Dispatchers.IO) {
            val result = getMovieDetailsUseCase(movieId)
            result
        }
        response.result?.let {
            _state.value = State.HideLoading
            _state.value = State.ShowMovieDetails(movie = it)
        }
        response.error?.let {
            _state.value = State.HideLoading
            setEffect(Effect.Error(it))
        }
    }

    fun saveMovie(movie: MovieDTO) = viewModelScope.launch {
        setEffect(Effect.ShowWaitDialog)
        val response = withContext(Dispatchers.IO){
            saveMovieUseCase.invoke(movie)
        }
        response.result?.let {
            setEffect(Effect.MovieSaved(it))
        }
        response.error?.let {
            setEffect(Effect.Error(it))
        }
        setEffect(Effect.HideWaitDialog)
    }

    sealed class State {
        object ShowLoading : State()
        object HideLoading : State()
        data class ShowMovieDetails(val movie: MovieDetailsDTO) : State()
    }

    sealed interface Effect {
        object ShowWaitDialog : Effect
        data class MovieSaved(val movie: MovieDTO) : Effect
        object HideWaitDialog : Effect
        data class Error(val error: String) : Effect
    }
}

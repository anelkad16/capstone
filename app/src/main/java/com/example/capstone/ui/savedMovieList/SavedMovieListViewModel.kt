package com.example.capstone.ui.savedMovieList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.capstone.domain.model.MovieDTO
import com.example.capstone.domain.usecase.DeleteMovieUseCase
import com.example.capstone.domain.usecase.GetSavedMovieListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SavedMovieListViewModel @Inject constructor(
    private val getSavedMovieListUseCase: GetSavedMovieListUseCase,
    private val deleteMovieUseCase: DeleteMovieUseCase
) : ViewModel() {

    private var _state = MutableStateFlow<State>(State.ShowLoading)
    val state: StateFlow<State> = _state

    private val _effect: Channel<Effect> = Channel()
    val effect = _effect.receiveAsFlow()

    init {
        getMovieList()
    }

    private fun setEffect(effectValue: Effect) {
        viewModelScope.launch { _effect.send(effectValue) }
    }

    fun getMovieList() = viewModelScope.launch {
        val response = withContext(Dispatchers.IO) {
            getSavedMovieListUseCase.invoke()
        }
        response.result?.let {
            _state.value = State.HideLoading
            _state.value = State.SavedMovieList(it)
        }
        response.error?.let {
            _state.value = State.HideLoading
            setEffect(Effect.Error(it))
        }
    }


    fun deleteMovie(movieId: Int) = viewModelScope.launch {
        setEffect(Effect.ShowWaitDialog)
        val response = withContext(Dispatchers.IO) {
            deleteMovieUseCase.invoke(movieId)
        }
        response.result?.let {
            setEffect(Effect.HideWaitDialog)
            setEffect(Effect.MovieDeleted(it))
        }
        response.error?.let {
            setEffect(Effect.HideWaitDialog)
            setEffect(Effect.Error(it))
        }
    }

    sealed class State {
        object ShowLoading : State()
        object HideLoading : State()
        data class SavedMovieList(val movies: List<MovieDTO>) : State()
    }

    sealed interface Effect {
        object ShowWaitDialog : Effect
        data class MovieDeleted(val movieId: Int) : Effect
        object HideWaitDialog : Effect
        data class Error(val error: String) : Effect
    }
}

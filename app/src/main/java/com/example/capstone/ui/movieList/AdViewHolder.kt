package com.example.capstone.ui.movieList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.capstone.R
import com.example.capstone.databinding.AdItemBinding
import com.example.capstone.domain.model.AdDTO

class AdViewHolder(
    private val adItemBinding: AdItemBinding
) : RecyclerView.ViewHolder(adItemBinding.root) {

    companion object {
        fun create(parent: ViewGroup): AdViewHolder {
            val binding = AdItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            return AdViewHolder(binding)
        }
    }

    fun bind(ad: AdDTO) {
        adItemBinding.title.text = ad.title
        adItemBinding.description.text = ad.description
        Glide
            .with(adItemBinding.imageView.context)
            .load(ad.image)
            .apply(RequestOptions.bitmapTransform(RoundedCorners(10)))
            .placeholder(R.drawable.progress_animation)
            .error(R.drawable.baseline_image_24)
            .into(adItemBinding.imageView)
    }
}
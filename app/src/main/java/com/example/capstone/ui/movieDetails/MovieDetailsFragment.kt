package com.example.capstone.ui.movieDetails

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.capstone.R
import com.example.capstone.databinding.FragmentMovieDetailsBinding
import com.example.capstone.domain.model.MovieDetailsDTO
import com.example.capstone.ui.delegates.DialogDelegate
import com.example.capstone.ui.delegates.WaitDialogDelegate
import com.example.capstone.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MovieDetailsFragment : Fragment(R.layout.fragment_movie_details),
    DialogDelegate by WaitDialogDelegate() {

    private var binding: FragmentMovieDetailsBinding? = null
    private val viewModel: MovieDetailsViewModel by viewModels()
    private val args: MovieDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieDetailsBinding.bind(view)
        viewModel.getMovie(args.id)
        bindViews()
        setupObservers()
        registerWaitDialogDelegate(this)
    }

    private fun bindViews() {
        binding?.backButton?.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setupObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MovieDetailsViewModel.State.HideLoading -> {
                    binding?.progressBar?.isVisible = false
                }

                is MovieDetailsViewModel.State.ShowLoading -> {
                    binding?.progressBar?.isVisible = true
                }

                is MovieDetailsViewModel.State.ShowMovieDetails -> {
                    bindMovie(state.movie)
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.effect.collect { effect ->
                when (effect) {
                    MovieDetailsViewModel.Effect.ShowWaitDialog -> {
                        showWaitDialog()
                    }

                    MovieDetailsViewModel.Effect.HideWaitDialog -> {
                        hideWaitDialog()
                    }

                    is MovieDetailsViewModel.Effect.MovieSaved -> {
                        Toast.makeText(
                            context,
                            getString(R.string.movie_saved_title, effect.movie.title),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    is MovieDetailsViewModel.Effect.Error -> {
                        Toast.makeText(
                            context,
                            effect.error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }
    }

    private fun bindMovie(movieDetails: MovieDetailsDTO) {
        binding?.apply {
            textviewTitle.text = movieDetails.title
            textviewDescription.text = movieDetails.overview
            if (movieDetails.tagline.isNotEmpty()) tagline.text =
                tagline.context.getString(R.string.tagline, movieDetails.tagline)
            releaseDate.text =
                releaseDate.context.getString(R.string.premiere, movieDetails.releaseDate)
            runtime.text = runtime.context.getString(
                R.string.runtime,
                movieDetails.runtime / 60,
                movieDetails.runtime % 60
            )
            if (movieDetails.revenue > 0) revenue.text =
                revenue.context.getString(R.string.revenue, movieDetails.revenue / 1000000)

            Glide
                .with(imageview.context)
                .load(Constants.IMAGE_URL + movieDetails.posterPath)
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.baseline_image_24)
                .into(imageview)

            Glide
                .with(imageview.context)
                .load(Constants.IMAGE_URL + movieDetails.backdropPath)
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.baseline_image_24)
                .into(imageview2)

            saveButton.isVisible = true
            saveButton.setOnClickListener { viewModel.saveMovie(movieDetails.toMovie()) }
        }
    }
}
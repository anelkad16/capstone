package com.example.capstone.ui.movieList

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.capstone.R
import com.example.capstone.databinding.FragmentMovieListBinding
import com.example.capstone.ui.delegates.DialogDelegate
import com.example.capstone.ui.delegates.WaitDialogDelegate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MovieListFragment : Fragment(R.layout.fragment_movie_list),
    DialogDelegate by WaitDialogDelegate() {

    private var binding: FragmentMovieListBinding? = null
    private val movieAdapter by lazy {
        MovieListAdapter(
            onMovieClickListener = ::navigateToDetails,
            onSaveMovieListener = { viewModel.saveMovie(it) }
        )
    }
    private val viewModel: MovieListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentMovieListBinding.bind(view)
        registerWaitDialogDelegate(this)
        setupObserver()
        bindViews()
    }

    private fun bindViews() {
        binding?.apply {
            listView.animation = null
            listView.adapter = movieAdapter
            swipeRefresh.setOnRefreshListener {
                viewModel.getMovieList((1..10).random())
            }
            val languageCodes = resources.getStringArray(R.array.language_codes)
            languageSpinner.setSelection(languageCodes.indexOf(viewModel.getLanguage()))
            languageSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val selectedLanguage = languageCodes[position]
                    viewModel.setLanguage(selectedLanguage)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        }
    }

    private fun navigateToDetails(movieId: Int) {
        val bundle = Bundle().apply {
            putInt("id", movieId)
        }
        findNavController().navigate(
            R.id.action_movieListFragment_to_movieDetailsFragment,
            bundle
        )
    }

    private fun setupObserver() {
        viewModel.state.onEach { state ->
            when (state) {
                is MovieListViewModel.State.HideLoading -> {
                    binding?.progressBar?.isVisible = false
                }

                is MovieListViewModel.State.ShowLoading -> {
                    binding?.progressBar?.isVisible = true
                }

                is MovieListViewModel.State.ShowMovieList -> {
                    binding?.apply {
                        swipeRefresh.isRefreshing = false
                    }
                    (binding?.listView?.adapter as? MovieListAdapter)
                        ?.submitList(state.movieList)
                }
            }
        }.launchIn(lifecycleScope)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.effect.collect { effect ->
                when (effect) {
                    MovieListViewModel.Effect.ShowWaitDialog -> {
                        showWaitDialog()
                    }
                    MovieListViewModel.Effect.HideWaitDialog -> {
                        hideWaitDialog()
                    }
                    is MovieListViewModel.Effect.MovieSaved -> {
                        Toast.makeText(
                            context,
                            getString(R.string.movie_saved_title, effect.movie.title),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    is MovieListViewModel.Effect.Error -> {
                        Toast.makeText(
                            context,
                            effect.error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }
    }
}
